//
//  ThumbnailView.h
//  Fabia
//
//  Created by simpzan on 9/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define InitialScale 0.20
#define kPhotoViewDidZoomInNotification @"PhotoViewDidZoomInNotification"

@interface PhotoView : UIView <UIGestureRecognizerDelegate> {
    NSString *_textImageName;
	NSString *_bigImageName;
    UIImage *_image;
    
    CGAffineTransform previousTransform;
    CGPoint previousCenter;
    
    int padding;
}
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, assign) CGAffineTransform previousTransform;
@property (nonatomic, assign) CGPoint previousCenter;
@property (nonatomic, retain) NSString *textImageName;
@property (nonatomic, retain) NSString *bigImageName;

- (id)initWithImageFile:(NSString *)file;

@end
