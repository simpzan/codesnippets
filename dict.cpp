#include <iostream>
#include "dict.h"

using std::string;
using std::endl;
using std::cout;

Dict::Dict() {
	;
}

Dict::~Dict(){
	if(db)	sqlite3_close(db);
	if(dictfile)	fclose(dictfile);
}

bool Dict::load(const char *filepath){
	ifofilepath.assign(filepath);
	if(sqlite3_open_v2(ifofilepath.c_str(), &db, SQLITE_OPEN_READONLY, NULL) != SQLITE_OK) {
		printf("open failed: %s\n", ifofilepath.c_str());
		return false;
	}

	string filename(ifofilepath);
	filename.replace(filename.rfind('.'), 5, ".dict");
	dictfile = fopen(filename.c_str(), "rb");
	if (!dictfile) {
		return false;
	}
	cout << ifofilepath << " opened" << endl;
	return true;
}

bool Dict::search(const char *aQuery, std::vector<std::string> &wordlist, bool prefix){
	sqlite3_stmt *stmt;
	wordlist.clear();
	string sql = "select * from dictIdx where headword like ? order by headword asc";
	if(sqlite3_prepare_v2(db, sql.c_str(), sql.length(), &stmt, NULL) != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", sqlite3_errmsg(db));
		return false;
	}
	string query(aQuery);
	if (prefix)	query += "%";
	sqlite3_bind_text(stmt, 1, query.c_str(), query.length(), NULL);
	while(sqlite3_step(stmt) == SQLITE_ROW){
		char *word =(char *) sqlite3_column_text(stmt, 0);
		wordlist.push_back(word);
	}
	sqlite3_finalize(stmt);
	return true;
}

bool Dict::lookup(const char *query, std::string &definition){
	sqlite3_stmt *stmt;
	long offset, length;
	string sql = "select * from dictIdx where headword=?";
	if(sqlite3_prepare_v2(db, sql.c_str(), sql.length(), &stmt, NULL) != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", sqlite3_errmsg(db));
		definition.clear();
		return false;
	}
	sqlite3_bind_text(stmt, 1, query, strlen(query), NULL);
	if(sqlite3_step(stmt) == SQLITE_ROW){
		offset = sqlite3_column_int64(stmt, 1);
		length = sqlite3_column_int64(stmt, 2);
	}
	sqlite3_finalize(stmt);
	cout << "offset:" << offset << " length:" << length << endl;
	
	char *dat =(char *) malloc(length);
	fseek(dictfile, offset, 0);
	fread(dat, 1, length, dictfile);
	definition.assign(dat, length);
	free(dat);
	return true;
}