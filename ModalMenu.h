//
//  ModalMenu.h
//  iDictionary
//
//  Created by simpzan on 2/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModalMenu : NSObject {
}

+ (NSUInteger) selectMenuFromItems:(NSArray *)menuItems showIn:(UIView *)showInView;

@end
