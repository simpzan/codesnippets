//
//  PhoneSearch.m
//  iDictionary
//
//  Created by simpzan on 2/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PhoneSearch.h"


@implementation PhoneSearch

void addressBookChangeCallback (ABAddressBookRef addressBook, CFDictionaryRef info, void *context){
	NSLog(@"call back");
//	[self loadPhoneData];
}

- (void) loadPhoneData {
	if (!phoneData){
		phoneData = [[NSMutableDictionary alloc]initWithCapacity:100];
		ABAddressBookRegisterExternalChangeCallback(addressBook, addressBookChangeCallback, NULL);
	}else {
		[phoneData removeAllObjects];
	}
	
	CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
	for (id aPeople in (NSArray*) allPeople) {
		CFStringRef name = ABRecordCopyValue(aPeople, kABPersonLastNameProperty);
		if (!name) {
			name = CFSTR(" ");
			name = CFRetain(name);
		}
		NSMutableSet *phoneSet = [[NSMutableSet alloc]initWithCapacity:3];
		ABMultiValueRef phones = ABRecordCopyValue(aPeople, kABPersonPhoneProperty);
		for (CFIndex i = 0; i < ABMultiValueGetCount(phones); i++) {
			CFStringRef phoneNumber = ABMultiValueCopyValueAtIndex(phones, i);
			[phoneSet addObject:(NSString *)phoneNumber];
			CFRelease(phoneNumber);
		}
		CFRelease(phones);
		[phoneData setObject:phoneSet forKey:(NSString *)name];
		[phoneSet release];
		CFRelease(name);
	}
	CFRelease(allPeople);
}

- (id) init{
	if (self = [super init]) {
		addressBook = ABAddressBookCreate();
		[self loadPhoneData];
		NSLog(@"AB init");
	}
	return self;
}

- (void) dealloc{
	NSLog(@"AB dealloc");
	CFRelease(addressBook);
	[phoneData release];
	[super dealloc];
}

- (NSArray *) searchWithNumberString:(NSString *)numberString{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self contains %@", numberString];
	NSMutableArray *ret = [NSMutableArray arrayWithCapacity:5];
	for (NSString *name in [phoneData allKeys]) {
		for (NSString *phone in [phoneData objectForKey:name]) {
			if ([predicate evaluateWithObject:phone]) {
				[ret addObject:[NSString stringWithFormat:@"%@ | %@", name, phone]];
			}
		}
	}
	return ret;
}

@end
