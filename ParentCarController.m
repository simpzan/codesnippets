    //
//  ParentCarController.m
//  YMSKODA
//
//  Created by yappam on 11-9-27.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ParentCarController.h"
#import "Notifications.h"

@implementation ParentCarController
@synthesize toolBar;
@synthesize hintImageView;
@synthesize carView;


#pragma mark Touches 


- (void)changeImage:(int)mode {
    if(mode==1)
	{
		currentIndex++;
	} else {
		currentIndex--;
	}
	
	if (currentIndex>=36) {
		currentIndex = 0;
	}
	if (currentIndex<0) {
		currentIndex = 36-1;
	}
    
	NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"360-00%03d", currentIndex]	ofType:@"jpg"];
	
	[carView setImage:[UIImage imageWithContentsOfFile:path]];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (hintImageView) {
        [hintImageView removeFromSuperview];
        self.hintImageView = nil;
    }
    UITouch *aTouch = [touches anyObject];
    if (aTouch.tapCount == 2) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
    }
    
    startPoint = previousPoint = [aTouch locationInView:carView];

    swipeCar = CGRectContainsPoint(carRect, startPoint);
    singleTap = YES;
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    singleTap = NO;
    CGPoint p = [[touches anyObject] locationInView:carView];
    
    if (swipeCar) {
        int d = p.x - previousPoint.x;
        if (d>6) {
            [self changeImage:1];
        } else if(d<-6) {
            [self changeImage:0];
        }
        previousPoint = p;
    } else {
        if (swiped) {
            return;
        }
        
        if ((startPoint.x - p.x) > kPanSpanMinimium) {
            swiped=YES;
            [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationLeftMove object:self];
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *theTouch = [touches anyObject];
    if (theTouch.tapCount == 1) {
        [self performSelector:@selector(singleTapAction:) withObject:nil afterDelay:0.3f];
    } else if (theTouch.tapCount == 2) {
        // Double-tap: increase image size by 10%"
        [self doubleTapAction:nil];
    }
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    currentIndex = 30;
    carRect = self.hintImageView.frame;
//    [self initGesture];
}

-(void)singleTapAction:(UITapGestureRecognizer *)sender{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
	[UIView animateWithDuration:0.3f
                     animations:^{
                         toolBar.alpha =toolBar.alpha ? 0 : 1;
                     }];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlaySound object:self];
}

-(void)doubleTapAction:(id)sender{
    NSLog(@"double tap");
	 [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDoubleTap object:self];
}

- (void)panPiece:(UIPanGestureRecognizer *)gestureRecognizer  
{  
    if (swiped) {
        return;
    }
    CGPoint p = [gestureRecognizer translationInView:gestureRecognizer.view.superview];
    //		NSLog(@"%lf", p.x);
    if (-p.x > kPanSpanMinimium) {
        swiped=YES;
        [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationLeftMove object:self];
    } 
}  

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

-(void)initGesture{
	UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panPiece:)];  
    [panGesture setMaximumNumberOfTouches:1];  
    [panGesture setDelegate:self];  
    [self.view addGestureRecognizer:panGesture];  
    [panGesture release];  
	
	UITapGestureRecognizer *singTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapAction:)];
	singTap.numberOfTouchesRequired=1;
	singTap.numberOfTapsRequired=1;
    
	UITapGestureRecognizer *doubleTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapAction:)];
	doubleTap.numberOfTouchesRequired=1;
	doubleTap.numberOfTapsRequired=2;
	[singTap requireGestureRecognizerToFail:doubleTap];
    
	[self.view addGestureRecognizer:singTap];
	[singTap release];
	
	[self.view addGestureRecognizer:doubleTap];
	[doubleTap release];	
}


-(void)systemItemClick:(id)sender{
	[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOpenShareDialog object:sender];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlaySound object:self];

}
-(void)mainMenuBarItemClick:(id)sender{
	[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOpenMenuDialog object:sender];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlaySound object:self];

}
-(void)bookDrawingItemClick:(id)sender{
	[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOpenOrderDialog  object:sender];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlaySound object:self];

}

- (IBAction)openHelpPage:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOpenHelpPage  object:sender];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlaySound object:self];
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidDisappear:animated];
	swiped=NO;
}


- (void)viewDidUnload {
    [self setHintImageView:nil];
    [self setCarView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {

    [hintImageView release];
    [carView release];
    [super dealloc];
	[toolBar release];
}


@end
