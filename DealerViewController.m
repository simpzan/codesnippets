//
//  DealerViewController.m
//  Fabia
//
//  Created by simpzan on 9/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DealerViewController.h"
#import "GDataXMLNode.h"
#import "Dealer.h"
#import "Utilities.h"
//#import "OrderViewController.h"

@implementation DealerViewController
@synthesize toolbar = _toolbar;
@synthesize tableViewCell = _tableViewCell;
@synthesize previousSelectedProvince = _previousSelectedProvince;
@synthesize selectedProvinceImageView = _selectedProvinceImageView;
@synthesize tableView = _tableView;
@synthesize openProvincesButton = _openProvincesButton;
@synthesize provinceButtonsView = _provinceButtonsView;
@synthesize mainView = _mainView;

#pragma mark - Custom Methods

- (void) loadXMLData {
    _dealersMap = [[NSMutableDictionary alloc]init];
    _provinceKeys = [[NSMutableArray alloc]init];
    NSString *xmlfile = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"AppDealer.xml"];
    NSData *dat = [NSData dataWithContentsOfFile:xmlfile];
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:dat options:0 error:nil];
        
    NSArray *dealers = [[doc rootElement] elementsForName:@"dealer"];
    for (GDataXMLElement *element in dealers) {
        Dealer *dealer = [[Dealer alloc] init];
        int dealerId = [[[[element elementsForName:@"id"]lastObject] stringValue] intValue];
        dealer.Id = dealerId;
        NSString *province = [[[element elementsForName:@"shengfen"] lastObject] stringValue];
        if ([province hasPrefix:@"内蒙古"] || [province hasPrefix:@"黑龙江"]) {
            province = [province substringToIndex:3];
        } else {
            province = [province substringToIndex:2];
        }
        dealer.province = province;
        NSString *address = [[[element elementsForName:@"dizhi"] lastObject] stringValue];
        dealer.address = address;
        NSString *name = [[[element elementsForName:@"mingcheng"] lastObject]stringValue];
        dealer.name = name;
        NSString *serviceNumber = [[[element elementsForName:@"dianhuaFW"]lastObject]stringValue];
        dealer.phoneForService = serviceNumber;
        NSString *saleNumber = [[[element elementsForName:@"dianhuaXS"]lastObject]stringValue];
        dealer.phoneForSale = saleNumber;
        
        NSMutableArray *array = [_dealersMap objectForKey:province];
        if (![_provinceKeys containsObject:province]) {
            [_provinceKeys addObject:province];
        }
        if (!array) {
            array = [[[NSMutableArray alloc] init] autorelease];
            [_dealersMap setObject:array forKey:province];
        }
        [array addObject:dealer];
        [dealer release];
    }
    [doc release];
}



- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    Class viewClass = [touch.view class];
    if ([viewClass isSubclassOfClass:[UIControl class]]) {
        return NO;
    }
    return YES;
}



- (void)handleTap {
    [UIView animateWithDuration:0.3f
                     animations:^{
                         _toolbar.alpha = _toolbar.alpha ? 0 : 1;
                     }];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlaySound object:self];

}


- (void)handlePan:(UIPanGestureRecognizer *)sender {
    if (_swiping) {
        return;
    }
    UIView *view = sender.view;
    CGPoint p = [sender translationInView:view];
    //    NSLog(@"%lf", p.x);
    if (-p.x > kPanSpanMinimium) {
        _swiping = YES;
        [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationLeftMove object:self];
    } 
}


- (IBAction)selectProvince:(UIButton *)sender {
    _searching = NO;
    
    self.previousSelectedProvince.hidden = NO;
    sender.hidden = YES;
    
    NSString *selected = [sender titleForState:UIControlStateNormal];
    [_openProvincesButton setTitle:selected forState:UIControlStateNormal];
    //    _openProvincesButton.titleLabel.text = self.currentProvince = selected;
    [_tableView reloadData];
    
    NSString *filename = [NSString stringWithFormat:@"%@.png", selected];
    NSString *bundlePath = [[NSBundle mainBundle]bundlePath];
    NSString *filepath = [bundlePath stringByAppendingPathComponent:filename];
    UIImage *image = [UIImage imageWithContentsOfFile:filepath];
    NSAssert(image, @"image is nil");
    NSAssert(_selectedProvinceImageView, @"selected image view is nil");
    
    _selectedProvinceImageView.image = image;
    
    self.previousSelectedProvince = sender;
}

- (IBAction)openProvincesPopover:(UIButton *)sender {
    if (!_provincesPopoverController) {
        UITableViewController *tvc = [[UITableViewController alloc]initWithStyle:UITableViewStylePlain];
        tvc.tableView.delegate = self;
        tvc.tableView.dataSource = self;
        _provincesPopoverController = [[UIPopoverController alloc]initWithContentViewController:tvc];
        [tvc release];
        _provincesPopoverController.popoverContentSize = CGSizeMake(200, 400);
    }
    
    CGRect rect = sender.superview.frame;
    rect.size.width = 100;
    [_provincesPopoverController presentPopoverFromRect:CGRectMake(70, 120, 60, 20) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (IBAction)hideMap:(id)sender {
//	[self dismissModalViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationLeftMove object:self];
}


- (IBAction)openMenuDialog:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationOpenMenuDialog object:sender userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlaySound object:self];

}

- (IBAction)openOrderDialog:(id)sender {
    [_provincesPopoverController dismissPopoverAnimated:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationOpenOrderDialog object:sender userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlaySound object:self];

}

- (IBAction)openShareDialog:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationOpenShareDialog object:sender userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlaySound object:self];

}

- (IBAction)openHelpDialog:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOpenHelpPage  object:sender];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlaySound object:self];
}

#pragma mark - UISearchBar 

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [_searchResults removeAllObjects];
    for (NSArray *dealers in [_dealersMap allValues]) {
        for (Dealer *dealer in dealers) {
            if ([dealer.name rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound
                || [dealer.address rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [_searchResults addObject:dealer];
            }
        }
    }
    [_tableView reloadData];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    _searching = YES;
    _selectedProvinceImageView.image = nil;
    _previousSelectedProvince.hidden = NO;
    [_tableView reloadData];
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - UITableView


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView!=_tableView) {
        return [_provinceKeys count];
    }
    if (_searching) {
        return [_searchResults count];
    }
    NSArray *dealers = [_dealersMap objectForKey:[_openProvincesButton titleForState:UIControlStateNormal]];
    int count = [dealers count];
//   NSLog(@"number of rows %d", count);
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView!=_tableView) {
        static NSString *identifier = @"provinceIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier]autorelease];
        }
        cell.textLabel.text = [_provinceKeys objectAtIndex:indexPath.row];
        return cell;
    }
    
    static NSString *identifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        [[NSBundle mainBundle] loadNibNamed:@"TableViewCell" owner:self options:nil];
        cell = _tableViewCell;
    }
    if (indexPath.row) {
        UIImageView *iv = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"虚线.png"]];
        iv.frame = CGRectMake(0, 0, 271, 1);
        iv.tag = 90;
        [cell addSubview:iv];
        [iv release];
    } else {
        UIView *view = [cell viewWithTag:90];
        [view removeFromSuperview];
    }
    Dealer *dealer = nil;
    if (_searching) {
        dealer = [_searchResults objectAtIndex:indexPath.row];
    } else {
        NSMutableArray *dealers = [_dealersMap objectForKey:[_openProvincesButton titleForState:UIControlStateNormal]];
        dealer = [dealers objectAtIndex:indexPath.row];
    }
    
    UILabel *label = (UILabel *) [cell viewWithTag:11];
    label.text = dealer.name;
    label = (UILabel *) [cell viewWithTag:12];
    label.text = [@"地址：" stringByAppendingString:dealer.address];
    label = (UILabel *) [cell viewWithTag:13];
    label.text = dealer.phoneForSale;
    label = (UILabel *) [cell viewWithTag:14];
    label.text = dealer.phoneForService;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView!=_tableView) {
        NSString *province = [_provinceKeys objectAtIndex:indexPath.row];
        [_provincesPopoverController dismissPopoverAnimated:YES];
        
        for (UIView *view in _provinceButtonsView.subviews) {
            if (view.tag) {
                continue;
            }
            UIButton *btn = (UIButton *)view;
            if ([[btn titleForState:UIControlStateNormal] isEqualToString:province]) {
                [self selectProvince:btn];
                break;
            }
        }
    }
}

#pragma mark - Overridden

- (NSString *)nibName {
    return @"DealerViewController";
}

- (void)dealloc
{
    [_provinceKeys release];
    [_dealersMap release];
    [_searchResults release];
    [_provincesPopoverController release];
    
    [_tableViewCell release];
    [_selectedProvinceImageView release];
    [_previousSelectedProvince release];
    [_tableView release];
    [_openProvincesButton release];
    [_provinceButtonsView release];
    [_mainView release];
    [_toolbar release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    _searchResults = [[NSMutableArray alloc] init];
    [self loadXMLData];
    
    [_selectedProvinceImageView.superview bringSubviewToFront:_selectedProvinceImageView];
    _tableView.allowsSelection = NO;
    
    
    UITapGestureRecognizer *gr =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap)];
    gr.delegate = self;
    [self.view addGestureRecognizer:gr];
    [gr release];
    
    UIPanGestureRecognizer *gr2 = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePan:)];
    [self.view addGestureRecognizer:gr2];
    [gr2 release];
}

- (void)viewDidUnload
{
    [self setTableViewCell:nil];
    [self setSelectedProvinceImageView:nil];
    [self setPreviousSelectedProvince:nil];
    [self setTableView:nil];
    [self setOpenProvincesButton:nil];
    [self setProvinceButtonsView:nil];
    [_searchResults release];
    [_dealersMap release];
    [_provinceKeys release];
    [_provincesPopoverController release];
    
    [self setMainView:nil];
    [self setToolbar:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft
            || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
@end
