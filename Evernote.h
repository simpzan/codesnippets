//
//  Evernote.h
//  CocoaTest
//
//  Created by simpzan on 7/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class EDAMUserStoreClient;
@class EDAMNoteStoreClient;
@class EDAMNote;

@interface Evernote : NSObject {
	EDAMUserStoreClient *userStore;
	EDAMNoteStoreClient *noteStore;
	EDAMNote *_note;
	NSString *authToken;
	NSString *consumerKey;
	NSString *consumerSecret;
}
@property (nonatomic, retain) EDAMUserStoreClient *userStore;
@property (nonatomic, retain) EDAMNoteStoreClient *noteStore;
@property (nonatomic, retain) EDAMNote *note;
@property (nonatomic, retain) NSString *authToken;

- (id) initWithKey:(NSString *)aKey secret:(NSString *) aSecret;
- (BOOL)connectWithUsername:(NSString *)username password:(NSString *)password;
- (NSArray*) content;
- (BOOL) setContent:(NSArray *)list;
+ (Evernote *)sharedInstance;
@end
