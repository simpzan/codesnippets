//
//  ParentCarController.h
//  YMSKODA
//
//  Created by yappam on 11-9-27.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "Show360CarController.h"

@interface ParentCarController : UIViewController<UIGestureRecognizerDelegate> {
    CGRect carRect;
    CGPoint startPoint;
    CGPoint previousPoint;
    int currentIndex;
	
	BOOL swiped;
    BOOL swipeCar;
    BOOL singleTap;
    
	UIToolbar *toolBar;
    UIImageView *hintImageView;
    UIImageView *carView;
}
@property (nonatomic, retain) IBOutlet UIToolbar *toolBar;
@property (nonatomic, retain) IBOutlet UIImageView *hintImageView;
@property (nonatomic, retain) IBOutlet UIImageView *carView;

-(void)panPiece:(UIPanGestureRecognizer *)gestureRecognizer; 
-(void)singleTapAction:(UITapGestureRecognizer *)sender;
-(void)doubleTapAction:(id)sender;

-(void)initGesture;
//-(void)initToolBar;

-(IBAction)systemItemClick:(id)sender;
-(IBAction)mainMenuBarItemClick:(id)sender;
-(IBAction)bookDrawingItemClick:(id)sender;
- (IBAction)openHelpPage:(id)sender;
@end
