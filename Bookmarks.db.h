//
//  Favorites.h
//  iDictionary
//
//  Created by simpzan on 10-10-16.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//
#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface Bookmarks : NSObject {
	NSMutableArray *items;
	NSMutableDictionary *deletedItems;
	NSString *filePath;
	BOOL bChanged;
	sqlite3 *db;
	
	NSMutableArray *reviewQueue;
	NSMutableArray *newQueue;
	NSMutableArray *failedQueue;
	NSMutableArray *changedQueue;// pending to write back to db.
}

- (void) addItem:(NSString *)word;
- (void) removeItem:(NSString *)word;
- (void) removeItemAtIndex:(int)index;
- (void) clearAllItems;
- (int) count;
- (BOOL) containsItem:(NSString *)word;
- (NSString *) itemAtIndex:(int)index;
- (void) sortByMode:(int)mode;
- (void) sync;
- (void) save;
- (NSString *) next;
- (void) answer:(int)quality;
- (NSString *) stats;
@end
