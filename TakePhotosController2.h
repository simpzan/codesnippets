//
//  TakePhotosController2.h
//  TEST2
//
//  Created by simpzan on 10/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface TakePhotosController2 : UIViewController <UIAlertViewDelegate> {
    
    UIView *controls;
    UIImageView *stillImageView;
    UIImageView *carView;
    UIImageView *hintImageView;
    
    AVCaptureVideoPreviewLayer *prevLayer;
    AVCaptureSession *captureSession;
    AVCaptureStillImageOutput *stillImageOutput;
    
    CGPoint startPoint;
    int currentIndex;
    BOOL started;
}
@property (nonatomic, retain) IBOutlet UIView *controls;
@property (nonatomic, retain) IBOutlet UIImageView *stillImageView;
@property (nonatomic, retain) IBOutlet UIImageView *carView;
@property (nonatomic, retain) IBOutlet UIImageView *hintImageView;

- (IBAction)close:(id)sender;
- (IBAction)shot:(id)sender;
- (IBAction)start:(id)sender;

@end
