    //
//  ScrollController.m
//  Fabia
//
//  Created by simpzan on 10/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ScrollController.h"
#import "Utilities.h"
#import "PhotoView.h"
#import "ModalAlert.h"

#define kFullRect CGRectMake(0, 0, 1024, 768)

@implementation ScrollController
@synthesize currentPageIndex = currentPageIndex_;

- (BOOL)isDisplayingPageForIndex:(NSUInteger)index
{
    BOOL foundPage = NO;
    for (UIImageView *page in visiblePages_) {
        if (page.tag == index) {
            foundPage = YES;
            break;
        }
    }
    return foundPage;
}

- (UIImageView *)dequeueRecycledPage
{
    UIImageView *page = [recycledPages_ anyObject];
    if (page) {
        [[page retain] autorelease];
        [recycledPages_ removeObject:page];
    }
    return page;
}


- (void)configurePage:(UIImageView *)page forIndex:(NSUInteger)index
{
    page.tag = index;
    page.frame = CGRectMake(index*pageWidth_, 0, pageWidth_, pageHeight_);
	
	PhotoView *pv = [pages_ objectAtIndex:index];
	NSString *filename = pv.bigImageName;
	page.image = [UIImage imageWithContentsOfFile:filename];
}

- (UIImageView *)createImageView {
	UIImageView *page = [[[UIImageView alloc] init] autorelease];
	page.userInteractionEnabled = YES;
	UILongPressGestureRecognizer *gr = [[[UILongPressGestureRecognizer alloc]
										 initWithTarget:self
										 action:@selector(handleLongPress:)]
										autorelease];
	[page addGestureRecognizer:gr];
	UITapGestureRecognizer *gr2 = [[[UITapGestureRecognizer alloc]initWithTarget:self
																		  action:@selector(handleDoubleTap)]autorelease];
	gr2.numberOfTapsRequired = 2;
	[page addGestureRecognizer:gr2];
	return page;
}

- (void)tilePages 
{
    // Calculate which pages are visible
    CGRect visibleBounds = scrollView_.bounds;
    int firstNeededPageIndex = floorf(CGRectGetMinX(visibleBounds) / CGRectGetWidth(visibleBounds));
    int lastNeededPageIndex  = floorf((CGRectGetMaxX(visibleBounds)-1) / CGRectGetWidth(visibleBounds));
    firstNeededPageIndex = MAX(firstNeededPageIndex, 0);
    lastNeededPageIndex  = MIN(lastNeededPageIndex, pageCount_ - 1);
    
    // Recycle no-longer-visible pages 
    for (UIImageView *page in visiblePages_) {
        if (page.tag < firstNeededPageIndex || page.tag > lastNeededPageIndex) {
            [recycledPages_ addObject:page];
            [page removeFromSuperview];
        }
    }
    [visiblePages_ minusSet:recycledPages_];
    
    // add missing pages
    for (int index = firstNeededPageIndex; index <= lastNeededPageIndex; index++) {
        if (![self isDisplayingPageForIndex:index]) {
            UIImageView *page = [self dequeueRecycledPage];
            if (page == nil) {
                page = [self createImageView];
            }
            [self configurePage:page forIndex:index];
            [scrollView_ addSubview:page];
            [visiblePages_ addObject:page];
        }
    }    
}


#pragma mark - Save Photo


- (void)handleDoubleTap {
	[[NSNotificationCenter defaultCenter] postNotificationName:DismissScrollControllerNotification object:self];
}

- (void)handleLongPress:(UIGestureRecognizer*)sender {
	if (recognizing) {
		return;
	}
	recognizing = YES;
	if ([ModalAlert ask:@"保存图片"]) {
		UIImageView *iv = (UIImageView*) sender.view;
		UIImageWriteToSavedPhotosAlbum(iv.image, nil, nil, nil);
//        UIImageWriteToSavedPhotosAlbum(iv.image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);

	}
	recognizing = NO;
}


- (void)               image: (UIImage *) image
    didFinishSavingWithError: (NSError *) error
                 contextInfo: (void *) contextInfo{
    if (error) {
        NSLog(@"%@", error);
        [ModalAlert say:@"保存失败"];
    } else {
        NSLog(@"succeeded");
        [ModalAlert say:@"保存成功"];
    }
}

#pragma mark UIScrollView

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
	[self tilePages];
}

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	UIView *view = [self.view viewWithTag:11];
    [view removeFromSuperview];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	currentPageIndex_ = [scrollView currentPageIndex];
    PhotoView *page = [pages_ objectAtIndex:currentPageIndex_];
    
    UIImage *image = [UIImage imageWithContentsOfFile:page.textImageName];
    NSAssert(image, @"image not available");
    UIImageView *iv = [[UIImageView alloc]initWithImage:image];
    iv.frame = CGRectMake(pageWidth_/2, 0, pageWidth_, pageHeight_);
    iv.tag = 11;
    [self.view addSubview:iv];
    [iv release];
    
    [UIView beginAnimations:nil context:nil];
    iv.center = CGPointMake(pageWidth_/2, pageHeight_/2);
    [UIView commitAnimations];
}


#pragma mark Overridden

- (id) initWithPages:(NSArray *)pages {
	self = [super initWithNibName:nil bundle:nil];
	if (self) {
		pages_ = [pages retain];
		pageCount_ = [pages count];
		pageWidth_ = 1024;
		pageHeight_ = 768;
	}
	return self;
}

- (void) viewWillAppear:(BOOL)animated {
	scrollView_.contentOffset = CGPointMake(currentPageIndex_*pageWidth_, 0);
	[self tilePages];
    [self scrollViewDidEndDecelerating:scrollView_];
}

// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
	self.view = [[[UIView alloc]initWithFrame:kFullRect] autorelease];
    
	scrollView_ = [[UIScrollView alloc]initWithFrame:kFullRect];
	scrollView_.contentSize = CGSizeMake(1024*[pages_ count], 768);
	scrollView_.pagingEnabled = YES;
	scrollView_.delegate = self;
	[self.view addSubview:scrollView_];
    
    UIImageView *iv = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"header.png"]];
    iv.frame = CGRectMake(0, 0, 1024, 124);
    [self.view addSubview:iv];
    [iv release];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:@"缩小.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(handleDoubleTap) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(975, 95, 50, 38);
    [self.view addSubview:btn];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	recycledPages_ = [[NSMutableSet alloc]init];
	visiblePages_ = [[NSMutableSet alloc]init];
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
	RELEASE(scrollView_);
	RELEASE(recycledPages_);
	RELEASE(visiblePages_);
	[super viewDidUnload];
}


- (void)dealloc {
	[scrollView_ release];
	[pages_ release];
	[recycledPages_ release];
	[visiblePages_ release];
    [super dealloc];
}


@end
