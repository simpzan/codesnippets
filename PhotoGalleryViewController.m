//
//  PhotoGalleryViewController.m
//  Fabia
//
//  Created by simpzan on 9/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PhotoGalleryViewController.h"
#import "PhotoView.h"
#import "Utilities.h"
#import "ModalAlert.h"
#import "ScrollController.h"

#define Center CGPointMake(512, 410)

#define LEFTCenter CGPointMake(326, 432)

#define LEFTPoint1 CGPointMake(176, 358)
#define LEFTPoint2 CGPointMake(466, 360)
#define LEFTPoint3 CGPointMake(166, 540)
#define LEFTPoint4 CGPointMake(440, 540)

#define RIGHTPoint1 CGPointMake(805, 355)
#define RIGHTPoint3 CGPointMake(803, 532)

#define WIDTH 1024
#define HEIGHT 768

#define kToolbarHideDelay 3.0f


@implementation PhotoGalleryViewController
@synthesize pewImageView = _pewImageView;
@synthesize scrollView = _scrollView;
@synthesize hiddenView = _hiddenView;
@synthesize saveDialog = _saveDialog;
@synthesize toolbar = _toolbar;

- (IBAction)openMenuDialog:(id)sender {   
    [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationOpenMenuDialog object:sender userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlaySound object:self];

}

- (IBAction)openOrderDialog:(UIBarButtonItem *)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationOpenOrderDialog object:sender userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlaySound object:self];

}

- (IBAction)openShareDialog:(UIBarButtonItem *)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationOpenShareDialog object:sender userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlaySound object:self];

}

- (IBAction)openHelpDialog:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOpenHelpPage  object:sender];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlaySound object:self];

}

#pragma mark - Touch 

- (void)handleTap:(UITapGestureRecognizer *)sender {
    [UIView animateWithDuration:0.3f animations:^{
        _toolbar.alpha = _toolbar.alpha ? 0 : 1;
    }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlaySound object:self];

}

- (void)handlePan:(UIPanGestureRecognizer *)sender {
	if (panned)	return;
    CGPoint p = [sender translationInView:sender.view];
    if (-p.x > kPanSpanMinimium) {
		panned = YES;
        [[NSNotificationCenter defaultCenter]postNotificationName:kNotificationLeftMove object:self];
    } 
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    Class panClass = [UIPanGestureRecognizer class];
    if ([[gestureRecognizer class]isSubclassOfClass:panClass] || [[otherGestureRecognizer class]isSubclassOfClass:panClass]) {
        return NO;
    }
    if (otherGestureRecognizer.view == self.view) {
        return NO;
    }
    return YES;
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (touch.view != self.view) {
        return NO;
    }
    return YES;
}

#pragma mark - Zoom In/Out ScrollView

// zoom out, called when the back button clicked.
- (IBAction)zoomOut:(id)sender {
	ScrollController *controller = (ScrollController *) self.modalViewController;
	int currentPageIndex = controller.currentPageIndex;
	[controller dismissModalViewControllerAnimated:NO];
	
    PhotoView *currentPage = [_pages objectAtIndex:currentPageIndex];
    
    // position current page back
	[currentPage.superview bringSubviewToFront:currentPage];
    currentPage.center = CGPointMake(WIDTH/2, HEIGHT/2);
	currentPage.transform = CGAffineTransformIdentity;
    
    
    // zoom out the page
    [UIView beginAnimations:nil context:nil];
    
    currentPage.transform = currentPage.previousTransform;
    currentPage.center = currentPage.previousCenter;
    
    [UIView commitAnimations];

    // prepare other pages.
    for (PhotoView *page in _pages) {
        if (page == currentPage) {
            continue;
        }
        page.center = page.previousCenter;
        page.transform = page.previousTransform;
    }
}

// called after the page is zoomed in. 
- (void) prepareScrollView:(NSNotification *)nofitication {
	ScrollController *controller = [[ScrollController alloc]initWithPages:_pages];
	PhotoView *currentPage = [nofitication object];
	controller.currentPageIndex = [_pages indexOfObject:currentPage];
	[self presentModalViewController:controller animated:NO];
	[controller release];
}


#pragma mark - Overridden

- (NSString *)nibName {
    return @"PhotoGalleryViewController";
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_pages release];
    [_pewImageView release];
    [_scrollView release];
    [_hiddenView release];
    [_saveDialog release];
    [_toolbar release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - View lifecycle

- (void) viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	panned = NO;
}

- (void)createPhotoViews {
    double angles[7] = {
        -0.523591, -0.385853, 0.267669, -0.043301, 0.034314, -0.294305, -0.474334,
    };
    int angleIndex = 0;
    
	NSArray *centerPoints = [NSArray arrayWithObjects:
                             [NSValue valueWithCGPoint:LEFTCenter],
                             [NSValue valueWithCGPoint:LEFTPoint1],
                             [NSValue valueWithCGPoint:LEFTPoint2],
                             [NSValue valueWithCGPoint:LEFTPoint3],
                             [NSValue valueWithCGPoint:LEFTPoint4],
                             nil];
	
    
    NSArray *pics = [@"materials/photos/硬" contentOfDirectoryInBundle];
	
    int index = 0;
    for (NSString *pic in pics) {
        if ([pic hasSuffix:@"png"]) {
            continue;
        }
		
        PhotoView *tv = [[PhotoView alloc]initWithImageFile:pic];
        tv.previousCenter = [[centerPoints objectAtIndex:index++] CGPointValue];
		tv.center = Center;
		
        CGAffineTransform trans = CGAffineTransformMakeRotation(angles[angleIndex++]);
        tv.transform = CGAffineTransformScale(trans, InitialScale, InitialScale);
        tv.previousTransform = tv.transform;
        
        [self.view insertSubview:tv aboveSubview:_pewImageView];
        [_pages addObject:tv];
        [tv release];
    }
	
    centerPoints = [NSArray arrayWithObjects:
					[NSValue valueWithCGPoint:RIGHTPoint1],
					[NSValue valueWithCGPoint:RIGHTPoint3],
					nil];
    pics = [@"materials/photos/in" contentOfDirectoryInBundle];
    index = 0;
    for (NSString *file in pics) {
        if ([file hasSuffix:@"png"]) {
            continue;
        }
		
        PhotoView *tv = [[PhotoView alloc]initWithImageFile:file];
		tv.center = Center;
        tv.previousCenter = [[centerPoints objectAtIndex:index++] CGPointValue];
        
        CGAffineTransform trans = CGAffineTransformMakeRotation(angles[angleIndex++]);
        tv.transform = CGAffineTransformScale(trans, InitialScale, InitialScale);
        tv.previousTransform = tv.transform;
        
        [self.view insertSubview:tv aboveSubview:_pewImageView];
        [_pages addObject:tv];
        [tv release];
    }
	
}

- (void)addGestureRecognizers {
	UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    gr.delegate = self;
    [self.view addGestureRecognizer:gr];
    [gr release];
	
	UIPanGestureRecognizer *gr5 = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePan:)];
    [self.view addGestureRecognizer:gr5];
    gr5.delegate = self;
    [gr5 release];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _pages = [[NSMutableArray alloc]init];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(prepareScrollView:) name:kPhotoViewDidZoomInNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(zoomOut:) name:DismissScrollControllerNotification object:nil];
	[self createPhotoViews];
    _pageCount = [_pages count];
    _scrollView.contentSize = CGSizeMake(_pageCount * WIDTH, HEIGHT);
    
	[self addGestureRecognizers];
	
	
    PhotoView *tv = [_pages objectAtIndex:0];
	tv.transform = CGAffineTransformIdentity;
	
	[UIView animateWithDuration:0.5f
					 animations:^{
						 tv.transform = tv.previousTransform;
					 } 
					 completion:^(BOOL finished) {
						 [UIView animateWithDuration:1.0f
											   delay:0.3f
											 options:UIViewAnimationCurveEaseIn
										  animations:^{
											  for (PhotoView *pv in _pages) {
												  pv.center = pv.previousCenter;
											  } 
										  } completion:nil];
					 }];
}

- (void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_pages release], _pages = nil;
    [self setPewImageView:nil];
    [self setScrollView:nil];
    [self setHiddenView:nil];
    [self setSaveDialog:nil];
    [self setToolbar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

@end
