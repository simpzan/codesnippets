//
//  Favorites.m
//  iDictionary
//
//  Created by simpzan on 10-10-16.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Bookmarks.h"
#import "Item.h"

@implementation Bookmarks


- (void) reloadDataFromDB{
	sqlite3_stmt *stmt;
	const char *sql = "select * from cards order by timeAdded desc";
	if(sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK){
		fprintf(stderr, "SQL: reloadDataFromDB statement preparation error: %s\n", sqlite3_errmsg(db));
	}
	NSString *word;
	double EF;
	int timeAdded, interval, lastQuality, nextRep, repCount;
	while (sqlite3_step(stmt)==SQLITE_ROW) {
		word = [NSString stringWithCString:(char *)sqlite3_column_text(stmt, 0) encoding:NSUTF8StringEncoding];
		timeAdded = sqlite3_column_int(stmt, 1);
		EF = sqlite3_column_double(stmt, 2);
		interval = sqlite3_column_int(stmt, 3);
		lastQuality = sqlite3_column_int(stmt, 4);
		nextRep = sqlite3_column_int(stmt, 5);
		repCount = sqlite3_column_int(stmt, 6);
		Item *item = [[Item alloc]initWithWord:word timeAdded:timeAdded EF:EF I:interval lastQuality:lastQuality nextRep:nextRep repCount:repCount];
		[items addObject:item];
		[item release];
	}
	sqlite3_finalize(stmt);
}

- (id) init{
	if (self = [super init]) {
		filePath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/bookmarks.db"] retain];
		bChanged = NO;
		items = [[NSMutableArray alloc]initWithCapacity:1000];
		deletedItems = [[NSMutableDictionary alloc]initWithCapacity:100];
		
		char *zErr;
		if (sqlite3_open_v2([filePath UTF8String], &db, SQLITE_OPEN_CREATE|SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
			fprintf(stderr, "SQL: init open database error: %s\n", sqlite3_errmsg(db));
			sqlite3_close(db);
			return self;
		}
		const char *sql = "create table if not exists cards (word text unique, timeAdded int,"
		"EF float not null default 2.5, interval int, lastQuality int not null default -1, nextRep int, repCount int)";
		if (sqlite3_exec(db, sql, NULL, NULL, &zErr) != SQLITE_OK) {
			if (zErr) {
				fprintf(stderr, "SQL: create table error: %s\n", zErr);
				sqlite3_free(zErr);
			}
		}
		[self reloadDataFromDB];
	}
	return self;
}

- (void) dealloc{
	NSLog(@"Bookmarks dealloc");
	
	[deletedItems release];
	[items release];
	[filePath release];
	sqlite3_close(db);
	[super dealloc];
}

- (void) save{
	NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/deleted.plist"];
	[deletedItems writeToFile:path atomically:YES];
	
 	if (!changedQueue)	return;
	sqlite3_exec(db, "begin", NULL, NULL, NULL);
	const char *sql = "update cards set EF=?1, interval=?2, lastQuality=?3, nextRep=?4, repCount=?5 where word=?6";
	sqlite3_stmt *stmt;
	if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK) {
		fprintf(stderr, "save statement preparation error: %s\n", sqlite3_errmsg(db));
	}
	const char *text;
	for (Item *item in changedQueue) {
		sqlite3_bind_double(stmt, 1, item.EF);
		sqlite3_bind_int(stmt, 2, item.interval);
		sqlite3_bind_int(stmt, 3, item.lastQuality);
		sqlite3_bind_int(stmt, 4, item.nextRep);
		sqlite3_bind_int(stmt, 5, item.repCount);
		text = [item.word UTF8String];
		sqlite3_bind_text(stmt, 6, text, strlen(text), NULL);
		if(sqlite3_step(stmt)!=SQLITE_DONE){
			printf("update failed");
		}
		sqlite3_reset(stmt);
	}
	sqlite3_finalize(stmt);
	sqlite3_exec(db, "commit", NULL, NULL, NULL);

	[reviewQueue release];
	reviewQueue = nil;
	[newQueue release];
	newQueue = nil;
	[failedQueue release];
	failedQueue = nil;
	[changedQueue release];
	changedQueue = nil;
	NSLog(@"saved");
}

#pragma mark -
#pragma mark Query

- (void) addItem:(NSString *)word{
	bChanged = YES;
	int timeAdded = [[NSDate date]timeIntervalSince1970];
	Item *item = [[[Item alloc]initWithWord:word timeAdded:timeAdded] autorelease];
	[items removeObject:item];
	[items insertObject:item atIndex:0];

	sqlite3_stmt *stmt;
	const char *sql = "insert into cards (word, timeAdded) values (?, ?)";
	if(sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, NULL) != SQLITE_OK){
		fprintf(stderr, "SQL: addItem statement preparation error: %s\n", sqlite3_errmsg(db));
	}
	const char *text = [word UTF8String];
	sqlite3_bind_text(stmt, 1, text, strlen(text), NULL);
	sqlite3_bind_int(stmt, 2, timeAdded);
	sqlite3_step(stmt);
	sqlite3_finalize(stmt);
}

- (void) removeItem:(NSString *)word{
	bChanged = YES;
	Item *item = [[[Item alloc]initWithWord:word timeAdded:0] autorelease];
	[items removeObject:item];
	[deletedItems setValue:[NSNumber numberWithDouble:[[NSDate date]timeIntervalSince1970]] forKey:word];
	
	sqlite3_stmt *stmt;
	const char *sql = "delete from cards where word=?";
	if(sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, NULL) != SQLITE_OK){
		fprintf(stderr, "SQL: removeItem statement preparation error: %s\n", sqlite3_errmsg(db));
	}
	const char *text = [word UTF8String];
	sqlite3_bind_text(stmt, 1, text, strlen(text), NULL);
	sqlite3_step(stmt);
	sqlite3_finalize(stmt);
}

- (void) removeItemAtIndex:(int)index{
	[self removeItem:[self itemAtIndex:index]];
}

- (void) clearAllItems{
	[items removeAllObjects];
	bChanged = YES;
}

- (BOOL) containsItem:(NSString *)word{
	Item *item = [[[Item alloc]initWithWord:word timeAdded:0] autorelease];
	return [items containsObject:item];
}

- (int) count{
	return [items count];
}

- (NSString *) itemAtIndex:(int)index{
	if (index >= [items count]) return nil;
	Item *item = [items objectAtIndex:index];
	return item.word;
}

- (void) sortByMode:(int)mode{
	if (mode==0) {
		[items sortUsingSelector:@selector(compare:)];
	}else if (mode == 1) {
		[items sortUsingSelector:@selector(compareAlphabetically:)];
	}else {
		[items sortUsingSelector:@selector(compareByNextRep:)];
	}

}

#pragma mark -
#pragma mark I/O

- (void) sync{
	NSString *syncPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/dic.txt"];
	NSString *tmp = [[NSString alloc]initWithContentsOfFile:syncPath encoding:NSUTF8StringEncoding error:nil];
	NSArray *lines = [tmp componentsSeparatedByString:@"\n"];
	[tmp release];
	
	sqlite3_exec(db, "begin", NULL, NULL, NULL);
	const char *insertStm = "insert into cards (word, timeAdded) values (?, ?)";
	sqlite3_stmt *stmt;
	if (sqlite3_prepare_v2(db, insertStm, -1, &stmt, NULL) != SQLITE_OK) {
		fprintf(stderr, "Sync statement preparation error: %s\n", sqlite3_errmsg(db));
	}
	
	const char *text;
	int i = 0;
	for (i=0; i<[lines count]; ++i) {
		NSString *line = [lines objectAtIndex:i];
		if (![line length]) break;
		NSArray *words = [line componentsSeparatedByString:@"\t"];
		text = [[words objectAtIndex:0] UTF8String];
		sqlite3_bind_text(stmt, 1, text, strlen(text), NULL);
		sqlite3_bind_int(stmt, 2, [[words objectAtIndex:1] intValue]);
		sqlite3_step(stmt);
		sqlite3_reset(stmt);
	}
	sqlite3_finalize(stmt);
	sqlite3_exec(db, "commit", NULL, NULL, NULL);

	[items sortUsingSelector:@selector(compare:)];
	for (; i<[lines count]; ++i) {
		NSString *line = [lines objectAtIndex:i];
		if ([line length]) {
			NSArray *words = [line componentsSeparatedByString:@"\t"];
			NSNumber *val = [NSNumber numberWithInteger:[[words objectAtIndex:1] integerValue]];
			[deletedItems setObject:val forKey:[words objectAtIndex:0]];
		}
	}
	
	[self reloadDataFromDB];
}

- (BOOL) queue{
	reviewQueue = [[NSMutableArray alloc]initWithCapacity:100];
	int today = [[NSDate date]timeIntervalSince1970]/86400;
	for (Item *item in items) {
		if (item.nextRep && item.nextRep <= today && item.lastQuality) {
			[reviewQueue addObject:item];
		}
	}
	
	newQueue = [[NSMutableArray alloc]initWithCapacity:50];
	for (Item *item in items) {
		if ([newQueue count]==2){
			break;
		}else if (!item.repCount) {
			[newQueue addObject:item];
		}
	}
	
	failedQueue = [[NSMutableArray alloc]initWithCapacity:50];
	for (Item *item in items) {
		if (item.lastQuality==0) {
			[failedQueue addObject:item];
		}
	}
	
	changedQueue = [[NSMutableArray alloc]initWithCapacity:200];
	return YES;
}

- (NSString *) next{
	if (!reviewQueue)	[self queue];
	Item *ret = nil;
	if ([reviewQueue count]) {
		ret = [reviewQueue objectAtIndex:0];
		[reviewQueue removeObjectAtIndex:0];
	}else if ([newQueue count]) {
		ret = [newQueue objectAtIndex:0];
		[newQueue removeObjectAtIndex:0];
	}else if ([failedQueue count]) {
		ret = [failedQueue objectAtIndex:0];
		[failedQueue removeObjectAtIndex:0];
	}
	if (ret)	[changedQueue addObject:ret];
	return ret.word;
}

- (void) answer:(int)quality{
	Item *item = [changedQueue lastObject];
	if (!item.repCount) {// first rate.
		int intervals[4] = {0, 1, 4, 7};
		item.interval = intervals[quality];
		item.EF = 2.5;
	} else {
		if (quality == 0) {// initial learning phase. straight 0s.
			item.EF -= 0.32;
			item.interval = 0;
		} else if (item.lastQuality == 0) {
			item.interval = 1;
		} else if (quality == 1) {
			item.EF -= 0.14;
			item.interval *= item.EF;
		} else if (quality == 2) {
			item.interval *= item.EF;
		} else {
			item.EF += 0.1;
			item.interval *= item.EF;
		}
	}
	if (item.EF<1.3)	item.EF = 1.3;
	if (!quality)	[failedQueue addObject:item];
	item.lastQuality = quality;
	item.nextRep = [[NSDate date]timeIntervalSince1970]/86400 + item.interval;
	++item.repCount;
}

- (NSString *) stats{
//	NSMutableDictionary *ret = [NSMutableDictionary dictionaryWithCapacity:5];
//	[ret setObject:[NSNumber numberWithInt:[reviewQueue count]] forKey:@"reviewCount"];
//	[ret setObject:[NSNumber numberWithInt:[newQueue count]] forKey:@"newCount"];
//	[ret setObject:[NSNumber numberWithInt:[failedQueue count]] forKey:@"failedCount"];
//	return ret;
	Item *item = [changedQueue lastObject];
	return [NSString stringWithFormat:@"%@<hr>Review:%d<br>New:%d<br>Failed:%d", 
			item, [reviewQueue count], [newQueue count], [failedQueue count]];
}

@end
