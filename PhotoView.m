//
//  ThumbnailView.m
//  Fabia
//
//  Created by simpzan on 9/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PhotoView.h"
#import "Utilities.h"

@interface PhotoView (Private)

- (void) handleDoubleTap:(UITapGestureRecognizer *)sender;

@end


@implementation PhotoView
@synthesize image = _image;
@synthesize previousTransform, previousCenter;
@synthesize textImageName = _textImageName;
@synthesize bigImageName = _bigImageName;


#pragma mark UIGestureRecognizer

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {

    if ([self.gestureRecognizers containsObject:otherGestureRecognizer]) {
        return YES;
    }
    return NO;

}


#pragma mark Custom Actions

- (void) handlePinch:(UIPinchGestureRecognizer *)gr {
    [self.superview bringSubviewToFront:self];

    if (gr.state == UIGestureRecognizerStateBegan) {
        previousTransform = self.transform;
    }else if(gr.state == UIGestureRecognizerStateEnded) {
		// get the total scale
        CGAffineTransform tran = self.transform;
        float scale = sqrt(tran.a*tran.a + tran.c*tran.c);
		
        if (scale > 0.6){ // if the scale is great enough. zoom in
            [self handleDoubleTap:nil];
        } else if (scale < InitialScale) { // if the scale is too little, reset to initial scale.
            [UIView beginAnimations:nil context:nil];
			
            float angle = atan2(tran.b, tran.a);
            tran = CGAffineTransformMakeRotation(angle);
            self.transform = CGAffineTransformScale(tran, InitialScale, InitialScale);
            
            [UIView commitAnimations];
        }
        return;
    }
	
    float scale = gr.scale;
    self.transform = CGAffineTransformScale(previousTransform, scale, scale);
}

- (void) handlePan:(UIPanGestureRecognizer *)gr {
    [self.superview bringSubviewToFront:self];
    
    if (gr.state == UIGestureRecognizerStateBegan) { // save the initial center.
        previousCenter = self.center;
    } 

    // get the moved path, the origin is the starting point.
    CGPoint translate = [gr translationInView:self.superview];  
	
    CGPoint newCenter = previousCenter;
    newCenter.x += translate.x;
    
    CGPoint p = [gr locationInView:self.superview];
    CGRect rect = CGRectMake(0, 188, 1024, 461);
    if (CGRectContainsPoint(rect, p)) {
        newCenter.y += translate.y;
    } else {
        newCenter.y = self.center.y;
    }
    
    self.center = newCenter;
}

// double tap, zoom in.
- (void) handleDoubleTap:(UITapGestureRecognizer *)sender {
    previousCenter = self.center;
    
	[UIView animateWithDuration:0.3f
					 animations:^{
                         self.center = CGPointMake(512, 384);
						 self.transform = CGAffineTransformIdentity;
					 } 
					 completion:^(BOOL finished){
						 [[NSNotificationCenter defaultCenter] postNotificationName:kPhotoViewDidZoomInNotification object:self];
					 }];
}

// single tap, bring self to front.
- (void) handleSingleTap:(UITapGestureRecognizer *)sender{
    [self.superview bringSubviewToFront:self];
}


#pragma mark Overridden

- (void)addGestureRecognizers {
	UIPinchGestureRecognizer *gr = [[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(handlePinch:)];
	gr.delegate = self;
	[self addGestureRecognizer:gr];
	[gr release];
	
	UIPanGestureRecognizer *gr2 = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePan:)];
	gr2.delegate = self;
	[self addGestureRecognizer:gr2];
	[gr2 release];
	
	UITapGestureRecognizer *gr3 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleDoubleTap:)];
	gr3.numberOfTapsRequired = 2;
	gr3.delegate = self;
	[self addGestureRecognizer:gr3];
	[gr3 release];
	
	UITapGestureRecognizer *gr4 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTap:)];
	gr4.numberOfTapsRequired = 1;
	gr4.delegate = self;
	[self addGestureRecognizer:gr4];
	[gr4 release];
}

- (id)initWithImageFile:(NSString *)file {
    UIImage *image =[UIImage imageWithContentsOfFile:file];
    NSAssert(image, @"image empty");
    CGSize size = image.size;
    self = [super initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    if (self) {
        _image = [image retain];
		_textImageName = [[[file substringToIndex:[file length]-5] stringByAppendingFormat:@"zi.png"] retain];
		_bigImageName = [file retain];
        padding = 14;
        self.exclusiveTouch = YES;
		self.backgroundColor = [UIColor whiteColor];

		CGRect rect = CGRectMake(0, 0, size.width, size.height);
		rect = CGRectInset(rect, padding, padding);
		UIImageView *iv = [[UIImageView alloc]initWithImage:_image];
		iv.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin
		| UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin 
		| UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
		iv.frame = rect;
		[self addSubview:iv];
		[iv release];
		
		[self addGestureRecognizers];
    }
    return self;
}


- (void)dealloc
{
	[_bigImageName release];
	[_textImageName release];
    [_image release];
    [super dealloc];
}

@end
