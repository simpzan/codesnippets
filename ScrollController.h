//
//  ScrollController.h
//  Fabia
//
//  Created by simpzan on 10/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define DismissScrollControllerNotification @"DismissScrollControllerNotification"


@interface ScrollController : UIViewController <UIScrollViewDelegate> {
	UIScrollView *scrollView_;
	NSMutableArray *pages_;
	int pageCount_;
	int currentPageIndex_;
	BOOL recognizing;
	int pageWidth_;
	int pageHeight_;
	NSMutableSet *recycledPages_;
	NSMutableSet *visiblePages_;
}
@property (nonatomic, assign) int currentPageIndex;

- (id) initWithPages:(NSArray *)pages;

@end
