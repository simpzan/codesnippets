//
//  PhotoGalleryViewController.h
//  Fabia
//
//  Created by simpzan on 9/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kNotificationBack @"notificationBack"
//#define kNotificationTap @"notificationTap"

@interface PhotoGalleryViewController : UIViewController <UIGestureRecognizerDelegate> {
    int _pageCount;
    NSMutableArray *_pages;
    
    BOOL longpressed;
    BOOL panned;
// UI
    UIImageView *_pewImageView;
    UIScrollView *_scrollView;
    UIView *_hiddenView;
    UIButton *_saveDialog;
    UIToolbar *_toolbar;
}
@property (nonatomic, retain) IBOutlet UIImageView *pewImageView;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIView *hiddenView;
@property (nonatomic, retain) IBOutlet UIButton *saveDialog;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbar;

- (IBAction)openMenuDialog:(id)sender;
- (IBAction)openOrderDialog:(UIBarButtonItem *)sender;
- (IBAction)openShareDialog:(UIBarButtonItem *)sender;
- (IBAction)openHelpDialog:(id)sender;

@end
