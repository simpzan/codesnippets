//
//  Utilities.h
//  CodeSnippet
//
//  Created by simpzan on 9/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Category

@interface UIView(UIViewAddition)

- (UIImage *) generateImage;

@end


@interface UIImage(UIImageAddition)

- (UIImage *)imageInRect:(CGRect)rect;

@end



CGFloat DistanceBetweenTwoPoints(CGPoint point1,CGPoint point2);

float randomNumber(float bottom, float top);

void playSound(NSURL *fileURL);