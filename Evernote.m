//
//  Evernote.m
//  CocoaTest
//
//  Created by simpzan on 7/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Evernote.h"
#import "THTTPClient.h"
#import "TBinaryProtocol.h"
#import "UserStore.h"
#import "NoteStore.h"

#import "RegexKitLite.h"

#define kNoteGuid @"noteGUID"
#define kUserStoreURI @"https://sandbox.evernote.com/edam/user"
#define kNoteStoreURIBase @"http://sandbox.evernote.com/edam/note/"
#define kNotebookName @"iDictionary"
#define kNoteTitle @"iDictionary.Bookmarks"

@implementation Evernote
@synthesize authToken;
@synthesize userStore, noteStore;
@synthesize note = _note;

- (id) initWithKey:(NSString *)aKey secret:(NSString *) aSecret{
	self = [super init];
	if (self) {
		consumerKey = [aKey retain];
		consumerSecret = [aSecret retain];
	}
	return self;
}

- (void) dealloc{
	[userStore release];
	[noteStore release];
	[authToken release];
	[consumerKey release];
	[consumerSecret release];
	[super dealloc];
}

- (BOOL) connectWithUsername:(NSString *)username password:(NSString *)password{
	THTTPClient *userStoreHttpClient = [[[THTTPClient alloc]
										 initWithURL:[NSURL URLWithString:kUserStoreURI]] autorelease];
	TBinaryProtocol *userStoreProtocol = [[[TBinaryProtocol alloc]
										   initWithTransport:userStoreHttpClient] autorelease];
	self.userStore = [[[EDAMUserStoreClient alloc]
									   initWithProtocol:userStoreProtocol] autorelease];
	
	BOOL versionOk = [userStore checkVersion:@"Cocoa EDAMTest2" :
					  [EDAMUserStoreConstants EDAM_VERSION_MAJOR] :
					  [EDAMUserStoreConstants EDAM_VERSION_MINOR]];
	
	if (versionOk == NO)	return NO;
	
	EDAMAuthenticationResult* authResult = [userStore authenticate:username :password
																  :consumerKey :consumerSecret];
	EDAMUser *user = [authResult user];
	self.authToken = [authResult authenticationToken];
	NSLog(@"Authentication was successful for: %@", [user username]);
	NSLog(@"Authentication token: %@", authToken);
	NSURL *noteStoreUri =  [[[NSURL alloc]initWithString:[NSString stringWithFormat:@"%@%@",
														  kNoteStoreURIBase, [user shardId]] ]autorelease];
	THTTPClient *noteStoreHttpClient = [[[THTTPClient alloc]initWithURL:noteStoreUri] autorelease];
	TBinaryProtocol *noteStoreProtocol = [[[TBinaryProtocol alloc]
										   initWithTransport:noteStoreHttpClient] autorelease];
	self.noteStore = [[[EDAMNoteStoreClient alloc]
									   initWithProtocol:noteStoreProtocol] autorelease];
	return YES;
}


EDAMNotebook *findNotebook(EDAMNoteStoreClient *noteStore, NSString *authToken){
	NSArray *notebooks = [noteStore listNotebooks:authToken];
	NSLog(@"Found %d notebooks", [notebooks count]);
	for (int i = 0; i < [notebooks count]; i++)
	{
		EDAMNotebook* notebook = (EDAMNotebook*)[notebooks objectAtIndex:i];
		if ([notebook.name isEqualToString:kNotebookName]) {
			return notebook;
		}
		NSLog(@" * %@", [notebook name]);
	}
	return nil;
}

- (NSArray *) content {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSString *noteGuid = [defaults objectForKey:kNoteGuid];
	if (noteGuid) {
		@try{
			self.note = [noteStore getNote:authToken :noteGuid :0 :0 :0 :0];
		} @catch ( NSException *e){
			;
		}
	}
	if (!self.note) {
		EDAMNotebook *myNotebook = findNotebook(noteStore, authToken);
		if (myNotebook == nil) {
			myNotebook = [[[EDAMNotebook alloc]init] autorelease];
			myNotebook.name = kNotebookName;
			myNotebook = [noteStore createNotebook:authToken :myNotebook];
			NSLog(@"Notebook created");
		}
		
		if (myNotebook == nil) {
			NSLog(@"Notebook open failed.");
			return NO;
		}
		
		EDAMNoteFilter *noteFilter = [[[EDAMNoteFilter alloc]init]autorelease];
		noteFilter.words = kNoteTitle;
		noteFilter.notebookGuid = myNotebook.guid;
		EDAMNoteList *noteList = [noteStore findNotes:authToken :noteFilter :0 :1];
		if (noteList.totalNotes == 1) {
			self.note = [noteList.notes lastObject];
		} else {
			EDAMNote *myNote = [[[EDAMNote alloc] init] autorelease];
			myNote.notebookGuid = myNotebook.guid;
			myNote.title = kNoteTitle;
			self.note = [noteStore createNote:authToken :myNote];
		}
		[defaults setObject:self.note.guid forKey:kNoteGuid];
	} else {
		
	}
	
	if (self.note == nil) {
		NSLog(@"Note open failed.");
		return NO;
	}
	NSArray *list = nil;
	NSString *noteContent = [noteStore getNoteContent:authToken :self.note.guid];
	CFShow(noteContent);
		list = [noteContent componentsMatchedByRegex:@"<div>(.*?)</div>" capture:1];
	return list;
}

- (BOOL) setContent:(NSArray *)list {
	NSMutableString *enNote = [[NSMutableString alloc]init];
	for (NSString *line in list) {
		[enNote appendFormat:@"<div>%@</div>", line];
	}
	self.note.content = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\
<!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml2.dtd\">\
<en-note>%@</en-note>", enNote];
	[enNote release];
	self.note = [noteStore updateNote:authToken :self.note];
	if (self.note == nil) {
		NSLog(@"update note failed");
		return NO;
	}
	return YES;
}

static Evernote *sharedInstance = nil;
+ (Evernote *) sharedInstance {
	if (!sharedInstance) {
		sharedInstance = [[Evernote alloc]initWithKey:@"simpzan" secret:@"5141830b3cc7b7db"];
	}
	return sharedInstance;
}

@end
