//
//  ModalMenu.m
//  iDictionary
//
//  Created by simpzan on 2/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ModalMenu.h"

@interface ModalMenuDelegate : NSObject <UIActionSheetDelegate>
{
	CFRunLoopRef currentLoop;
	NSUInteger index;
}
@property (assign) NSUInteger index;
@end

@implementation ModalMenuDelegate
@synthesize index;

-(id) initWithRunLoop: (CFRunLoopRef)runLoop 
{
	if (self = [super init]) currentLoop = runLoop;
	return self;
}

// User pressed button. Retrieve results
- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	self.index = buttonIndex;
	CFRunLoopStop(currentLoop);
}

@end



@implementation ModalMenu

+ (NSUInteger) selectMenuFromItems:(NSArray *)menuItems  showIn:(UIView *)showInView{
	CFRunLoopRef currentLoop = CFRunLoopGetCurrent();
	
	ModalMenuDelegate *mmdelegate = [[ModalMenuDelegate alloc]initWithRunLoop:currentLoop];
	UIActionSheet *actionSheet = [[UIActionSheet alloc]init];
	actionSheet.delegate = mmdelegate;
	for (NSString *item in menuItems) {
		[actionSheet addButtonWithTitle:item];
	}
	[actionSheet addButtonWithTitle:@"Cancel"];
	actionSheet.cancelButtonIndex = actionSheet.numberOfButtons-1;
	[actionSheet showInView:showInView];

	// Wait for response
	CFRunLoopRun();
	
	// Retrieve answer
	NSUInteger answer = mmdelegate.index;
	[actionSheet release];
	[mmdelegate release];
	return answer;
}

@end
