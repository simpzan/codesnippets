//
//  PhoneSearch.h
//  iDictionary
//
//  Created by simpzan on 2/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

@interface PhoneSearch : NSObject {
	ABAddressBookRef addressBook;
	NSMutableDictionary *phoneData;
}

- (NSArray *) searchWithNumberString:(NSString *)numberString;

@end
