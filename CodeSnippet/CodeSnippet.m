//
//  CodeSnippet.m
//  CodeSnippet
//
//  Created by simpzan on 9/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CodeSnippet.h"

#pragma mark - Exception

void uncaughtExceptionHandler(NSException *exception) {
	NSArray *arr = [exception callStackSymbols];
	NSString *reason = [exception reason];
	NSString *name = [exception name];
	NSString *urlStr = [NSString stringWithFormat:@"mailto://xqualia@gmail.com?subject=bug_report&"
						"body=感谢您的配合!<br><br><br>错误详情:<br>%@<br>--------------------------<br>%@<br>"
						"---------------------<br>%@", name,reason,[arr componentsJoinedByString:@"<br>"]];
	NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	[[UIApplication sharedApplication] openURL:url];
}


@implementation CodeSnippet

- (void) testMethod {
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
}

@end
