//
//  main.m
//  CodeSnippet
//
//  Created by simpzan on 9/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - interface

@interface AppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *_window;
}

@end


#pragma mark - implementation

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    _window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    [_window makeKeyAndVisible];
    return YES;
}


- (void)dealloc {
    [_window release];
    [super dealloc];
}

@end


#pragma mark - main

int main(int argc, char *argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, @"AppDelegate");
    [pool release];
    return retVal;
}
