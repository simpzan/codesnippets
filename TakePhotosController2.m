//
//  TakePhotosController2.m
//  TEST2
//
//  Created by simpzan on 10/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <ImageIO/ImageIO.h>
#import "TakePhotosController2.h"
#import "ModalAlert.h"
#import "Utilities.h"

#define kFullRect CGRectMake(0, 0, 1024, 768)

@implementation TakePhotosController2
@synthesize controls;
@synthesize stillImageView;
@synthesize carView;
@synthesize hintImageView;


- (void) initSession {
    AVCaptureDeviceInput *captureInput = [AVCaptureDeviceInput deviceInputWithDevice:
                                          [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo]  
                                                                               error:nil];
    captureSession = [[AVCaptureSession alloc] init];
    captureSession.sessionPreset = AVCaptureSessionPresetPhoto;
    [captureSession addInput:captureInput];
    
    prevLayer = [[AVCaptureVideoPreviewLayer alloc]initWithSession:captureSession];
    prevLayer.frame = CGRectMake(0, 0, 1024, 768);
    prevLayer.orientation = AVCaptureVideoOrientationLandscapeLeft;
    
    [self.view.layer insertSublayer:prevLayer atIndex:0];
    
    
    stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    
    NSDictionary *outputSettings = [[[NSDictionary alloc] initWithObjectsAndKeys:
                                     
                                     AVVideoCodecJPEG, AVVideoCodecKey, nil] autorelease];
    
    [stillImageOutput setOutputSettings:outputSettings];
    [captureSession addOutput:stillImageOutput];
    [captureSession startRunning];
}

- (AVCaptureConnection *)theAVCaptureConnection {
    for (AVCaptureConnection *connection in stillImageOutput.connections) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] ) {
                if (self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
                    connection.videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
                } else {
                    connection.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
                }
                return connection;
            }
        }
    }
    return nil;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex) {
        UIImage *image = [self.view generateImage];
        
        UIImageWriteToSavedPhotosAlbum(image, self, @selector( image: didFinishSavingWithError: contextInfo:), nil);
    }
    
    stillImageView.alpha = 0;
    controls.alpha = 1;
}

- (void) saveConfirm {
    UIAlertView *alertView = [[[UIAlertView alloc]initWithTitle:@"保存？" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil] autorelease];
    
    [alertView show];
    
    stillImageView.alpha = 1;
    controls.alpha = 0;
}

- (void)               image: (UIImage *) image
    didFinishSavingWithError: (NSError *) error
                 contextInfo: (void *) contextInfo {
    CFShow(error);
    
}

- (void)changeImage:(int)mode {
    if(mode==1)
	{
		currentIndex++;
	} else {
		currentIndex--;
	}
	
	if (currentIndex>=36) {
		currentIndex = 0;
	}
	if (currentIndex<0) {
		currentIndex = 36-1;
	}
    
	NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"定位_00%03d", currentIndex]	ofType:@"png"];
	
	[carView setImage:[UIImage imageWithContentsOfFile:path]];
}

#pragma mark Touches 

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!started) {
        return;
    }
    startPoint = [[touches anyObject] locationInView:carView];
    if (hintImageView) {
        [hintImageView removeFromSuperview];
        self.hintImageView = nil;
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!started) {
        return;
    }
    CGPoint p = [[touches anyObject] locationInView:carView];
    int d = p.x - startPoint.x;
    if (d>6) {
        [self changeImage:1];
    } else if(d<-6) {
        [self changeImage:0];
    }
    startPoint = p;
}

#pragma mark Override 

- (NSString *)nibName {
    return @"TakePhotosController2";
}

- (void)dealloc
{
    if (started) {
        [captureSession stopRunning];
        [prevLayer release];
        [captureSession release];
        [stillImageOutput release];
    }
    
    [controls release];
    [stillImageView release];
    [carView release];
    [hintImageView release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.frame = kFullRect;
    currentIndex = 30;
}

- (void)viewDidUnload
{
    [captureSession stopRunning];
    [prevLayer release];
    [captureSession release];
    [stillImageOutput release];
    
    [self setControls:nil];
    [self setStillImageView:nil];
    [self setCarView:nil];
    [self setHintImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
        prevLayer.orientation = AVCaptureVideoOrientationLandscapeLeft;
        return YES;
    }
	else if (interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        prevLayer.orientation = AVCaptureVideoOrientationLandscapeRight;
		return YES;
	}
	return NO;
}

- (IBAction)close:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)shot:(id)sender {
    if (hintImageView) {
        [hintImageView removeFromSuperview];
        self.hintImageView = nil;
    }
    
    AVCaptureConnection *videoConnection = [self theAVCaptureConnection];
    
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler:
     
     ^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
         CFDictionaryRef exifAttachments = CMGetAttachment(imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
         if (exifAttachments)
         {
             NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
             UIImage *image = [[UIImage alloc] initWithData:imageData];
             
             stillImageView.image = image;
             
             [image release];
             [self performSelectorOnMainThread:@selector(saveConfirm) withObject:nil waitUntilDone:NO];
         }
         else
             NSLog(@"no attachments"); 
     }];
}

- (IBAction)start:(id)sender {
    [self initSession];

    controls.alpha = 1;
    started = YES;
    
    UIView *view = [self.view viewWithTag:88];
    [view removeFromSuperview];
}


@end
