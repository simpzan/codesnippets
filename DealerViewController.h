//
//  DealerViewController.h
//  Fabia
//
//  Created by simpzan on 9/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kNotificationStartAnimation @"notificationStartAnimation"

@interface DealerViewController : UIViewController <UITableViewDelegate
, UITableViewDataSource, UISearchBarDelegate, UIGestureRecognizerDelegate> {
    // Model
    BOOL _searching;
    BOOL _swiping;

    NSMutableArray *_provinceKeys;
    NSMutableDictionary *_dealersMap;
    NSMutableArray *_searchResults;
  
    // Controller
    UIPopoverController *_provincesPopoverController;
    UIPopoverController *_appointmentDialog;
    
    // View
    UIButton *_openProvincesButton;
    UIView *_provinceButtonsView;
    UIView *_mainView;
    UIButton *_previousSelectedProvince;
    UIImageView *_selectedProvinceImageView;
    UITableView *_tableView;

    UITableViewCell *_tableViewCell;
    UIToolbar *_toolbar;
}
@property (nonatomic, retain) IBOutlet UITableViewCell *tableViewCell;
@property (nonatomic, retain) IBOutlet UIButton *previousSelectedProvince;
@property (nonatomic, retain) IBOutlet UIImageView *selectedProvinceImageView;
@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet UIButton *openProvincesButton;
@property (nonatomic, retain) IBOutlet UIView *provinceButtonsView;
@property (nonatomic, retain) IBOutlet UIView *mainView;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbar;

- (IBAction)selectProvince:(UIButton *)sender;
- (IBAction)openProvincesPopover:(UIButton *)sender;
- (IBAction)hideMap:(id)sender;
- (IBAction)openMenuDialog:(id)sender;
- (IBAction)openOrderDialog:(id)sender;
- (IBAction)openShareDialog:(id)sender;
- (IBAction)openHelpDialog:(id)sender;

@end
