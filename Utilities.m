//
//  Utilities.m
//  CodeSnippet
//
//  Created by simpzan on 9/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "Utilities.h"


#pragma mark - Image related

@implementation UIView(UIViewAddition)

- (UIImage *)generateImage {
    UIGraphicsBeginImageContext(self.frame.size);
	[self.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    return image;
}

@end

@implementation UIImage(UIImageAddition)

- (UIImage *)imageInRect:(CGRect)rect {
    CGImageRef image = self.CGImage;
    CGImageRef subImage = CGImageCreateWithImageInRect(image, rect);
    UIImage *ret = [UIImage imageWithCGImage:subImage];
    CGImageRelease(subImage);
    return ret;
}

@end



#pragma mark - Scalar related

CGFloat DistanceBetweenTwoPoints(CGPoint point1,CGPoint point2)
{
    CGFloat dx = point2.x - point1.x;
    CGFloat dy = point2.y - point1.y;
    return sqrt(dx*dx + dy*dy );
};

float randomNumber(float bottom, float top){
    float ran = ((float)rand())/RAND_MAX;
    ran *= (top - bottom);
    ran += bottom;
    return ran;
}


#include <AudioToolbox/AudioToolbox.h>

void playSound(NSURL *fileURL){
    SystemSoundID	soundFileObject;
    AudioServicesCreateSystemSoundID ((CFURLRef)fileURL, &soundFileObject);
    AudioServicesPlaySystemSound (soundFileObject);
}