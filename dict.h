#pragma once
#include <string>
#include <vector>
#include <sqlite3.h>

class Dict
{
public:
	Dict();
	~Dict();

	bool load(const char *filename);
	bool search(const char *query, std::vector<std::string> &wordlist, bool prefix = true);
	bool lookup(const char *query, std::string &definition);

	// properties.
	std::string bookname;
	long wordcount;
private:
	std::string ifofilepath;
	sqlite3 *db;
	FILE *dictfile;
};
